import React from 'react';
import { Box, Menu, Typography } from '@mui/material';

type FilterProps = {
    setFilter: (type: 'completed' | 'notCompleted' | '') => void;
    currentFilter: 'completed' | 'notCompleted' | '';
};

export const Filter = ({ setFilter, currentFilter }: FilterProps) => {
    const [anchorElType, setAnchorElType] = React.useState<any>();
    const handleClickOnType = (event: any) => {
        setAnchorElType(event.currentTarget);
    };

    const handleCloseType = () => {
        setAnchorElType(null);
    };

    const openType = (id: string) => {
        if (anchorElType) {
            return anchorElType.getAttribute('aria-controls') == id
                ? true
                : false;
        } else return false;
    };

    return (
        <>
            <Box
                id='basic-button-type'
                aria-controls='basic-menu-type'
                aria-haspopup='true'
                onClick={handleClickOnType}
                sx={{
                    cursor: 'pointer',
                    display: 'flex',
                    alignItems: 'center',
                    mx: 2,
                }}
            >
                Filter
            </Box>
            <Menu
                id='basic-menu'
                anchorEl={anchorElType}
                open={openType('basic-menu-type')}
                onClose={handleCloseType}
                MenuListProps={{
                    'aria-labelledby': 'basic-button-type',
                }}
            >
                <Box
                    sx={{
                        backgroundColor:
                            currentFilter == 'completed'
                                ? 'lightblue'
                                : 'white',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'flex-start',
                        fontWeight: 700,
                        fontSize: '13px',
                        mx: '20px',
                        my: '12px',
                    }}
                >
                    <Typography
                        sx={{
                            textAlign: 'center',
                            padding: '2px 10px',
                            borderRadius: '6px',
                            height: '30px',
                            mr: '7px',
                            cursor: 'pointer',
                        }}
                        onClick={() => setFilter('completed')}
                    >
                        Completed
                    </Typography>
                </Box>
                <Box
                    sx={{
                        backgroundColor:
                            currentFilter == 'notCompleted'
                                ? 'lightblue'
                                : 'white',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'flex-start',
                        fontWeight: 700,
                        fontSize: '13px',
                        mx: '20px',
                        my: '12px',
                    }}
                >
                    <Typography
                        sx={{
                            textAlign: 'center',
                            padding: '2px 10px',
                            borderRadius: '6px',
                            height: '30px',
                            mr: '7px',
                            cursor: 'pointer',
                            display: 'flex',
                            alignContent: 'center',
                        }}
                        onClick={() => setFilter('notCompleted')}
                    >
                        Not Completed
                    </Typography>
                </Box>
                <Box
                    sx={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'flex-start',
                        fontWeight: 700,
                        fontSize: '13px',
                        mx: '20px',
                        my: '12px',
                    }}
                >
                    <Typography
                        sx={{
                            textAlign: 'center',
                            padding: '2px 10px',
                            borderRadius: '6px',
                            height: '30px',
                            mr: '7px',
                            cursor: 'pointer',
                            display: 'flex',
                            alignContent: 'center',
                        }}
                        onClick={() => setFilter('')}
                    >
                        Clear Filter
                    </Typography>
                </Box>
            </Menu>
        </>
    );
};
