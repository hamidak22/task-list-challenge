import { useState } from 'react';
import { useToggle } from 'react-use';
import { v4 as uuidv4 } from 'uuid';
import {
    DialogTitle,
    Stack,
    DialogActions,
    Button,
    Dialog,
    Divider,
    TextField,
    Checkbox,
    FormControlLabel,
} from '@mui/material';
import { SingleTask } from 'types/global';

type Props = {
    open: boolean;
    onClose: () => void;
    onConfirm: (data: SingleTask) => void;
};

export function CustomModal({ open, onClose, onConfirm }: Props) {
    const [title, setTitle] = useState('');
    const [desc, setDesc] = useState('');
    const [completed, setCompleted] = useToggle(false);

    const handleOnConfirm = () => {
        onConfirm({
            id: uuidv4(),
            title: title,
            description: desc,
            completed: completed,
        });
        setTitle('');
        setDesc('');
        setCompleted(false);
    };

    return (
        <Dialog open={open} fullWidth maxWidth='sm' onClose={onClose}>
            <DialogTitle sx={{ display: 'flex', alignItems: 'center' }}>
                Add New Task
            </DialogTitle>

            <Stack spacing={3} sx={{ p: 3, pb: 0, mb: 3 }}>
                <TextField
                    placeholder='Place Name'
                    onChange={e => setTitle(e.target.value)}
                    value={title}
                />
                <TextField
                    placeholder='Place Name'
                    onChange={e => setDesc(e.target.value)}
                    value={desc}
                />
                <FormControlLabel
                    control={
                        <Checkbox value={completed} onChange={setCompleted} />
                    }
                    label={'Completed'}
                />
            </Stack>
            <Divider />
            <DialogActions>
                <Button variant='outlined' color='inherit' onClick={onClose}>
                    Cancel
                </Button>
                <Button
                    variant='contained'
                    sx={{ color: 'white' }}
                    onClick={handleOnConfirm}
                >
                    Add New Task
                </Button>
            </DialogActions>
        </Dialog>
    );
}
