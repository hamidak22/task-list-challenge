import { useState } from 'react';
import { useToggle } from 'react-use';
import { filter, findIndex } from 'lodash';
import { Box, Button, Stack, Typography, styled } from '@mui/material';
import { Theme } from '@mui/material/styles/createTheme';
import { useTheme } from '@mui/system';
import { SingleTask } from 'types/global';
import { Filter } from 'components/shared/Filter';
import List from './List';

export default function Main() {
    const [openModal, toggleModal] = useToggle(false);
    const [data, setData] = useState<SingleTask[]>([]);
    const [currentFilter, setCurrentFilter] = useState<
        'completed' | 'notCompleted' | ''
    >('');

    const theme = useTheme() as Theme;

    const APP_BAR_MOBILE = 64;
    const APP_BAR_DESKTOP = 92;

    const RootStyle = styled('div')({
        minHeight: '100%',
        height: '100vh',
        overflow: 'hidden',
        backgroundColor: theme.palette.grey[200],
    });

    const MainStyle = styled('div')(({ theme }) => ({
        flexGrow: 1,
        overflow: 'auto',
        minHeight: '100%',
        paddingTop: APP_BAR_MOBILE + 24,
        paddingBottom: theme.spacing(10),
        [theme.breakpoints.up('lg')]: {
            paddingTop: APP_BAR_DESKTOP + 24,
            paddingLeft: theme.spacing(20),
            paddingRight: theme.spacing(20),
        },
    }));

    const AddNewTask = (currentData: SingleTask) => {
        const newData = [...data, currentData];
        setData(newData);
        toggleModal();
    };

    const AddFilter = (type: 'completed' | 'notCompleted' | '') => {
        setCurrentFilter(type);
    };

    const ChangeStatus = (completed: boolean, id: string) => {
        const clonedData = JSON.parse(JSON.stringify(data));
        const currentIndex = findIndex(data, item => item.id == id);
        clonedData[currentIndex] = {
            ...clonedData[currentIndex],
            completed: completed,
        };
        setData(clonedData);
    };

    const FilteredData =
        currentFilter == 'completed'
            ? filter(data, (item: SingleTask) => item.completed)
            : currentFilter == 'notCompleted'
            ? filter(data, (item: SingleTask) => !item.completed)
            : data;

    return (
        <RootStyle>
            <MainStyle>
                <Stack
                    display={'flex'}
                    flexDirection={'row'}
                    justifyContent='space-between'
                    mb={3}
                    spacing={2}
                >
                    <Typography variant='h3' gutterBottom>
                        Tasks
                    </Typography>
                    <Box display={'flex'} flexDirection={'row'}>
                        <Filter
                            setFilter={AddFilter}
                            currentFilter={currentFilter}
                        />
                        <Button
                            variant='contained'
                            onClick={toggleModal}
                            sx={{ ml: 2 }}
                        >
                            Add New Task
                        </Button>
                    </Box>
                </Stack>
                <List
                    data={FilteredData}
                    AddNewTask={AddNewTask}
                    open={openModal}
                    toggleModal={toggleModal}
                    ChangeStatus={ChangeStatus}
                />
            </MainStyle>
        </RootStyle>
    );
}
