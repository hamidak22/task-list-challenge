import { map } from 'lodash';
import {
    Chip,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from '@mui/material';
import { SingleTask } from 'types/global';
import { CustomModal } from 'components/shared/Modal';

type Props = {
    data: SingleTask[];
    AddNewTask: (data: SingleTask) => void;
    open: boolean;
    toggleModal: () => void;
    ChangeStatus: (completed: boolean, id: string) => void;
};

export default function List({
    data,
    AddNewTask,
    open,
    toggleModal,
    ChangeStatus,
}: Props) {
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label='simple table'>
                <TableHead>
                    <TableRow>
                        <TableCell>Title</TableCell>
                        <TableCell align='right'>Description</TableCell>
                        <TableCell align='right'>Status</TableCell>
                        <TableCell align='right'>Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {map(data, (row, index) => (
                        <TableRow
                            key={row.title}
                            sx={{
                                '&:last-child td, &:last-child th': {
                                    border: 0,
                                },
                            }}
                        >
                            <TableCell component='th' scope='row'>
                                {row.title}
                            </TableCell>
                            <TableCell align='right'>
                                {row.description}
                            </TableCell>
                            <TableCell align='right'>
                                {row.completed ? 'Completed' : 'Not Completed'}
                            </TableCell>
                            <TableCell align='right'>
                                <Chip
                                    label={
                                        row.completed
                                            ? 'Uncomplete'
                                            : 'Complete'
                                    }
                                    color={
                                        row.completed ? 'warning' : 'success'
                                    }
                                    onClick={() =>
                                        ChangeStatus(!row.completed, row.id)
                                    }
                                />
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <CustomModal
                open={open}
                onClose={toggleModal}
                onConfirm={AddNewTask}
            />
        </TableContainer>
    );
}
